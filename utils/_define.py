########################################
#   Source Paths                       #
########################################
class Paths:
    path_db_retrieved = r".\db_retrieved.csv"
    path_db_processed = r".\db_processed.csv"

    test_path_db_retrieved = r".\_test_db_retrieved.csv"
    test_path_db_processed = r".\_test_db_processed.csv"


########################################
#   Database Headers                   #
########################################
POSITIVE_COLUMN = "sentiment-analysis_POSITIVE"
NEGATIVE_COLUMN = "sentiment-analysis_NEGATIVE"

# URLs
STEAM_REVIEWS_URL_TEMPLATE = "https://store.steampowered.com/appreviews/{app_id}"
REDDIT_NEW_POSTS_URL_TEMPLATE = "https://oauth.reddit.com/r/{subreddit}/new/?limit=100"

########################################
#   Retrieval utilities                #
########################################
OVERFLOW_COMMENT_COUNT = 100_000
MAX_COMMENTS_PER_REQUEST = 100
MAX_DAY_TIMEDELTA_PER_REQUEST = 10

########################################
#   Processing utilities                #
########################################
DEFAULT_SENTIMENT_MODEL = "distilbert-base-uncased-finetuned-sst-2-english"

########################################
#   Visualization utilities            #
########################################
SATURDAY = 5
SUNDAY = 6


class Color:
    POSITIVE = (0.4, 0.85, 0.65)
    NEUTRAL = (90 / 255, 138 / 255, 212 / 255)
    NEGATIVE = (0.9, 0.5, 0.4)

    WEEKEND = (0, 0.6, 1)


##################################################
#   Placeholders (TODO: convert to parameters)   #
##################################################
PLACEHOLDER_ASPECT_LIST = ["story", "gameplay"]
PLACEHOLDER_N_DAYS = 10

TASKS = ["sentiment"]


class SteamID:
    BORDERLANDS = 8980
    BORDERLANDS_3 = 397540


class Subreddit:
    BORDERLANDS = r"Borderlands"
    BORDERLANDS_2 = r"Borderlands2"
    BORDERLANDS_3 = r"borderlands3"
    WONDERLANDS = r"Wonderlands"
