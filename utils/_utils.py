import requests
import yaml


def get_config():
    with open("config.yaml") as fin:
        return yaml.safe_load(fin)


def authenticate_reddit():
    config = get_config()

    auth = requests.auth.HTTPBasicAuth(config["client_id"], config["secret_token"])

    data = {
        "grant_type": "password",
        "username": config["username"],
        "password": config["password"],
    }
    headers = {"User-Agent": "MyBot/0.0.1"}
    res = requests.post(
        "https://www.reddit.com/api/v1/access_token",
        auth=auth,
        data=data,
        headers=headers,
    )

    token = res.json()["access_token"]
    headers = {**headers, **{"Authorization": f"bearer {token}"}}
    requests.get("https://oauth.reddit.com/api/v1/me", headers=headers)

    return headers
