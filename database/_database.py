import os.path
from typing import List
from datetime import datetime, timedelta
import pandas as pd

from utils import Paths, MAX_DAY_TIMEDELTA_PER_REQUEST, get_config


class Document:
    def __init__(self, platform, datetime, language, text):
        self.platform = platform
        self.datetime = datetime
        self.language = language
        self.text = text

    def to_dict(self):
        return {
            "platform": self.platform,
            "datetime": self.datetime,
            "language": self.language,
            "text": repr(self.text),
        }


def get_retrieved_index_offset():
    if not os.path.isfile(Paths.path_db_retrieved):
        return 0

    with open(Paths.path_db_retrieved, encoding="utf8") as fin:
        for line in fin:
            pass

    return int(line.split()[0]) + 1


def add_documents_to_database(documents: List[Document]):
    documents = [doc.to_dict() for doc in documents]

    offset = get_retrieved_index_offset()
    new_retrieved_df = pd.DataFrame(documents)
    new_retrieved_df.index += offset

    # Skip the header if this is not the first append
    new_retrieved_df.to_csv(
        Paths.path_db_retrieved, mode="a", encoding="utf8", sep="\t", header=offset == 0
    )


def get_processed_dataframe():
    if is_first_processed_data():
        return pd.DataFrame()

    else:
        processed_df = pd.read_csv(
            Paths.path_db_processed, sep="\t", encoding="utf8", index_col=0
        )
        processed_df.index = processed_df.index.astype(int)
        return processed_df


def get_unprocessed_dataframe():
    retrieved_df = get_retrieved_dataframe()
    processed_df = get_processed_dataframe()

    # Find fresh indices (present in retrieved data, but absent from processed data)
    new_rows = sorted(list(set(retrieved_df.index) - set(processed_df.index)))

    # Initialize a new DataFrame that will be appended to file after
    return pd.DataFrame(index=new_rows)


def get_retrieved_dataframe():
    if not os.path.isfile(Paths.path_db_retrieved):
        empty_doc = Document(None, None, None, None)
        doc_keys = empty_doc.to_dict().keys()

        return pd.DataFrame(columns=doc_keys)

    else:
        return pd.read_csv(
            Paths.path_db_retrieved,
            sep="\t",
            encoding="utf8",
            index_col=0,
            parse_dates=["datetime"],
        )


def is_first_processed_data():
    """Figure out whether there is data saved without loading the entire file"""
    if not os.path.isfile(Paths.path_db_processed):
        return True

    with open(Paths.path_db_processed, encoding="utf8") as fin:
        for i, line in enumerate(fin):
            # If there is more than 1 line (headers) in the file, then there is data
            if i > 1:
                return False

    return True


def last_df_date(df):
    # If there is no previously downloaded data, we take all data up to the max date
    since_date = datetime.now() - timedelta(days=MAX_DAY_TIMEDELTA_PER_REQUEST)
    # Need to remove microseconds for datetime format consistency with other datetimes
    since_date -= timedelta(microseconds=since_date.microsecond)

    config = get_config()
    since_date_dict = {platform: since_date for platform in config["platforms"]}

    last_dates_df = df.groupby(by=["platform"])["datetime"].max()
    last_dates_dict = {
        index: date.to_pydatetime()
        for index, date in zip(last_dates_df.index, last_dates_df)
    }

    since_date_dict.update(last_dates_dict)
    return since_date_dict
