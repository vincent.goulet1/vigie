# Chitterboard

## Introduction

This project contains the code to create a live dashboard from public data gathered from social platforms where users comment on Gearbox products. Currently data is gathered from Reddit and Steam.

## Requirements

- I recommend using Anaconda to create a virtual environment for this project

## Getting started

- Clone this repository
- Navigate to the created folder, and create a conda environment from the `environment.yml` file

```
cd D:\git\chitterboard
```

## Retrieve, Process and Visualize

The pipeline is composed of three scripts that may be scheduled independently, namely:

- `retrieve.py` which gathers data from online comments via API calls
- `process.py` which infers information based on the text content of the online comments
- `visualize.py` which creates the visualization from the results of both previous steps
