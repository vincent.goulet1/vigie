import retrieve
import process
import visualize

retrieve.default_run()
process.default_run()
visualize.default_run()
