import logging
import pandas as pd
from transformers import pipeline

import database
from utils import PLACEHOLDER_ASPECT_LIST, Paths, DEFAULT_SENTIMENT_MODEL


def infer_aspect_based_sentiment(
    retrieved_df,
    processed_df,
    aspects=None,
    multilabel=False,
):
    # BROKEN DUE TO WIP REFACTOR AND TEMPORARY LACK OF USE
    # TODO: Finish refactor

    # Preprocess
    # Aspects are english words that capture sub-topics that may be mentioned in user feedback
    # Todo: replace aspects by command line args
    if aspects is None:
        aspects = PLACEHOLDER_ASPECT_LIST

    preprocessed = []
    for text in retrieved_df["text"]:
        for aspect in aspects:
            preprocessed.append(f"[CLS] {text} [SEP] {aspect} [SEP]")

    # Infer
    generator = pipeline(
        "sentiment-analysis",
        model="yangheng/deberta-v3-base-absa-v1.1",
        device=0,
    )

    print(f"Inferring for {len(preprocessed)} documents...")
    outputs = generator(preprocessed, top_k=3)
    print("Inferred!")

    # Store
    labels = list(generator.model.config.label2id.keys())
    results = {aspect: {label: [] for label in labels} for aspect in aspects}

    for i, text in enumerate(retrieved_df["text"]):
        for j, aspect in enumerate(aspects):
            output = outputs[len(aspects) * i + j]

            for label_and_score in output:
                results[aspect][label_and_score["label"]].append(
                    label_and_score["score"]
                )

                key = f"sentiment-{aspect}_{label_and_score['label']}"

                if key not in processed_df:
                    processed_df[key] = None
                processed_df[key][i] = label_and_score["score"]

    return processed_df


def infer_sentiment(retrieved_df, unprocessed_df, model_id=DEFAULT_SENTIMENT_MODEL):
    task = "sentiment-analysis"

    generator = pipeline(
        task,
        model=model_id,
    )

    # Only processing data marked as "english" for now
    # TODO: extend to more languages

    english_filter = retrieved_df.index[
        (retrieved_df["language"] == "english")
        & (retrieved_df["text"].str.contains("[A-Za-z]"))
    ]
    # Only keep the indices for new data
    english_filter = [idx for idx in english_filter if idx in unprocessed_df.index]

    if len(english_filter) == 0:
        logging.info("No new english text to process")
        return unprocessed_df

    outputs = generator(
        retrieved_df["text"].loc[english_filter].tolist(),
        truncation=True,
        top_k=2,
    )

    outputs_formatted_for_pandas = [
        {"_".join([task, pred["label"]]): pred["score"] for pred in output}
        for output in outputs
    ]

    outputs_df = pd.DataFrame(outputs_formatted_for_pandas, index=english_filter)
    processed_df = pd.concat([unprocessed_df, outputs_df], axis=1)

    return processed_df


def default_run():
    new_retrieved_df = database.get_retrieved_dataframe()
    unprocessed_df = database.get_unprocessed_dataframe()

    processed_df = infer_sentiment(new_retrieved_df, unprocessed_df)

    processed_df.to_csv(
        Paths.path_db_processed,
        mode="a",
        encoding="utf8",
        sep="\t",
        header=database.is_first_processed_data(),
    )


if __name__ == "__main__":
    default_run()
