import os
from utils import Paths
import retrieve, process, visualize

Paths.path_db_retrieved = Paths.test_path_db_retrieved
Paths.path_db_processed = Paths.test_path_db_processed


# delete test DB
def clear_test_artifacts():
    if os.path.isfile(Paths.test_path_db_retrieved):
        os.remove(Paths.test_path_db_retrieved)
    if os.path.isfile(Paths.test_path_db_processed):
        os.remove(Paths.test_path_db_processed)


def test_pipeline():
    # The first retrieve and process will create the database files
    print("Retrieving...")
    retrieve.default_run()
    print("Processing...")
    process.default_run()

    # The second retrieve and process will append to the database files
    print("Retrieving...")
    retrieve.default_run()
    print("Processing...")
    process.default_run()

    # Visualize
    print("Visualizing...")
    visualize.default_run()


if __name__ == "__main__":
    # The testing uses special test databases that allow us to test the first time a database is created
    # without affecting existing DB. We first make sure to clear potentially remaining artifacts.
    clear_test_artifacts()

    # The three pipeline scripts should be able to be ran one after the other
    test_pipeline()

    clear_test_artifacts()
