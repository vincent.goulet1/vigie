from datetime import datetime, timedelta, date

import numpy as np
import pandas as pd

from utils import (
    PLACEHOLDER_N_DAYS,
    SUNDAY,
    SATURDAY,
    Color,
    POSITIVE_COLUMN as POS_COL,
    NEGATIVE_COLUMN as NEG_COL,
)
import database

from matplotlib import pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
from matplotlib.patches import Rectangle


def get_sentiment_counts(dates):
    retrieved_df = database.get_retrieved_dataframe()
    processed_df = database.get_processed_dataframe()

    combined_df = pd.concat([retrieved_df, processed_df], axis=1)
    combined_df.dropna(inplace=True)

    # Create date column so that all comments on same day have equal date value (ignore hour, minute, etc)
    combined_df["date"] = combined_df["datetime"].apply(
        lambda _datetime: _datetime.date()
    )

    df_in_dates = combined_df[combined_df["date"].isin(dates)]

    # Multi-level indexing on date and whether positive
    date_results_count = df_in_dates.groupby(
        ["date", df_in_dates[POS_COL] > df_in_dates[NEG_COL]]
    ).count()["datetime"]

    pos_counts = [
        date_results_count.xs(True, level=1)[date] if date in date_results_count else 0
        for date in dates
    ]
    neg_counts = [
        date_results_count.xs(False, level=1)[date] if date in date_results_count else 0
        for date in dates
    ]

    return pos_counts, neg_counts


def plot_sentiment_count_last_n_days(ax, n_days):
    dates = [date.today() - timedelta(days=n_days - i) for i in range(n_days)]

    # Gather data
    positive_counts, negative_counts = get_sentiment_counts(dates)

    # Pre-format plot
    highlight_weekend(ax, dates)

    # Plot data
    plt.plot(dates, negative_counts, color=Color.NEGATIVE, label="Negative", lw=3)
    plt.plot(dates, positive_counts, color=Color.POSITIVE, label="Positive", lw=3)

    # Post-format plot
    # TODO autoformat platforms
    plt.title(
        f"Count of positive and negative reviews (last {n_days} days)\n "
        f"Platforms: Steam"
    )
    remove_frame(ax)
    plt.grid(axis="y", color="k", alpha=0.1)
    ax.tick_params(width=0, length=0)
    # Set the y-axis max to a multiple of 5 that is greater than 1.1 times the curve max
    y_axis_max = (max(positive_counts + negative_counts) * 1.1 // 5 + 1) * 5
    plt.ylim([0, y_axis_max])
    plt.xticks(dates, rotation=30)

    weekend_patch = mpatches.Patch(color=Color.WEEKEND, label="Weekend")
    handles, _ = ax.get_legend_handles_labels()
    plt.legend(handles=handles + [weekend_patch])


def plot_sentiment_percentage_last_n_days(ax, n_days):
    dates = [date.today() - timedelta(days=n_days - i) for i in range(n_days)]

    # Gather data
    positive_counts, negative_counts = get_sentiment_counts(dates)

    positive_percents = [
        100 * pos / (pos + neg) if pos + neg else 0
        for pos, neg in zip(positive_counts, negative_counts)
    ]
    negative_percents = [
        100 * -neg / (pos + neg) if pos + neg else 0
        for pos, neg in zip(positive_counts, negative_counts)
    ]

    # Pre-format plot
    highlight_weekend(ax, dates)

    # Plot data
    plt.bar(dates, negative_percents, color=Color.NEGATIVE, label="Negative")
    plt.bar(dates, positive_percents, color=Color.POSITIVE, label="Positive")

    # Post-format plot
    plt.title(
        f"Percentages of positive and negative reviews (last {n_days} days)\n "
        f"Platforms: Steam"
    )
    remove_frame(ax)
    plt.ylim([-100, 100])
    plt.xticks(dates, rotation=30)

    y_tick_values = np.linspace(-100, 100, 5)
    # TODO: Display negative y-tick labels as positive percentages
    ax.yaxis.set_major_formatter(mtick.PercentFormatter())
    plt.grid(axis="y", color="k", alpha=0.1)
    ax.tick_params(width=0, length=0)
    weekend_patch = mpatches.Patch(color=Color.WEEKEND, label="Weekend")
    handles, _ = ax.get_legend_handles_labels()
    plt.legend(handles=handles + [weekend_patch])
    # plt.legend()


def remove_frame(ax):
    """Hide the default black frame around the axis"""
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)


def highlight_weekend(ax, dates):
    """Draw rectangles aligned on weekends"""
    y0 = -110  # percent (the y-axis goes from -100% to +100%)
    width = 1  # day
    height = 220  # percent (the y-axis goes from -100% to +100%)

    for date in dates:
        weekday = date.weekday()

        if weekday in [SATURDAY, SUNDAY]:
            # Matplotlib uses their own representation for dates where the unit is days
            x0 = mdates.date2num(date) - 0.5
            rect = Rectangle(
                (x0, y0), width, height, facecolor=Color.WEEKEND, alpha=0.15
            )
            ax.add_patch(rect)


def default_run():
    plt.figure(figsize=(8, 12))

    # Add the line plot to figure
    line_ax = plt.subplot(2, 1, 1)
    plot_sentiment_count_last_n_days(line_ax, PLACEHOLDER_N_DAYS)

    # Add the bar plot to figure
    bar_ax = plt.subplot(2, 1, 2)
    plot_sentiment_percentage_last_n_days(bar_ax, PLACEHOLDER_N_DAYS)

    # Align the x-axis
    line_ax.set_xlim(bar_ax.get_xlim())

    # Display
    plt.tight_layout(pad=5)
    plt.show()


if __name__ == "__main__":
    default_run()
