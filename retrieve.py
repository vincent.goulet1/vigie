import requests
from tqdm import tqdm
from datetime import datetime, timedelta
import logging

from database import (
    Document,
    add_documents_to_database,
    get_retrieved_dataframe,
    last_df_date,
)
from utils import (
    SteamID,
    authenticate_reddit,
    Subreddit,
    STEAM_REVIEWS_URL_TEMPLATE,
    REDDIT_NEW_POSTS_URL_TEMPLATE,
    OVERFLOW_COMMENT_COUNT,
    MAX_COMMENTS_PER_REQUEST,
)

logging.basicConfig(level=logging.INFO)


def get_reddit_documents(subreddit, since_date):
    headers = authenticate_reddit()
    oldest_post_id = ""
    oldest_time = datetime.now()

    comment_count = 0
    all_reddit_documents = []

    while oldest_time >= since_date and comment_count < OVERFLOW_COMMENT_COUNT:
        if comment_count == 0:
            url = REDDIT_NEW_POSTS_URL_TEMPLATE.format(subreddit=subreddit)

        else:
            url = f"https://oauth.reddit.com/r/{subreddit}/new/?after={oldest_post_id}&limit={MAX_COMMENTS_PER_REQUEST}"

        res = requests.get(url, headers=headers)
        subreddit_posts_list = res.json()["data"]["children"]

        documents = [
            Document(
                platform="reddit",
                datetime=datetime.fromtimestamp(post["data"]["created_utc"]),
                language="english",
                text=post["data"]["selftext"],
            )
            for post in subreddit_posts_list
        ]

        # Filter out documents before "since_date"
        all_reddit_documents += [doc for doc in documents if doc.datetime > since_date]

        oldest_post_id = subreddit_posts_list[-1]["data"]["name"]
        oldest_time = datetime.fromtimestamp(
            subreddit_posts_list[-1]["data"]["created_utc"]
        )
        comment_count += MAX_COMMENTS_PER_REQUEST

    if comment_count > OVERFLOW_COMMENT_COUNT:
        logging.warning(
            f"Reddit retrieval exceeded the overflow limit of {OVERFLOW_COMMENT_COUNT}. "
            f"Some data may not have been fetched."
        )

    all_reddit_documents = sorted(all_reddit_documents, key=lambda doc: doc.datetime)

    logging.info(
        f"{len(all_reddit_documents)} new documents were retrieved from Reddit"
    )

    if len(all_reddit_documents):
        logging.info(f"The earliest is from {all_reddit_documents[0].datetime}")
        logging.info(f"The latest is from {all_reddit_documents[-1].datetime}")

    return all_reddit_documents


def get_steam_documents(app_id, since_date):
    url = STEAM_REVIEWS_URL_TEMPLATE.format(app_id=app_id)
    cursor = "*"

    # Documentation of steam reviews API parameters: https://partner.steamgames.com/doc/store/getreviews
    params = {
        "filter": "recent",
        "language": "all",  # Overriding default 'english'
        "purchase_type": "all",  # Overriding default 'steam'
    }

    params.update({"json": 1})

    # The API returns a maximum of 100 reviews per request. We use a cursor to get more.
    bar = tqdm()
    reviews = []
    comment_count = 0

    datetime_last = datetime.now()
    # We do not have the option to set a range of time, so we break when we reach
    while datetime_last >= since_date and comment_count < OVERFLOW_COMMENT_COUNT:
        bar.update()
        params["cursor"] = cursor.encode()
        params["num_per_page"] = min(
            MAX_COMMENTS_PER_REQUEST, OVERFLOW_COMMENT_COUNT - comment_count
        )

        comment_count += MAX_COMMENTS_PER_REQUEST

        response = requests.get(url, params, headers={"User-Agent": "Mozilla/5.0"})
        response = response.json()

        cursor = response["cursor"]
        reviews += response["reviews"]

        datetime_last = datetime.fromtimestamp(reviews[-1]["timestamp_created"])

    steam_documents = [
        Document(
            platform="steam",
            datetime=datetime.fromtimestamp(review["timestamp_created"]),
            language=review["language"],
            text=review["review"],
        )
        for review in reviews
    ]

    steam_documents = [doc for doc in steam_documents if doc.datetime > since_date]
    steam_documents = sorted(steam_documents, key=lambda doc: doc.datetime)

    logging.info(f"{len(steam_documents)} new documents were retrieved from Steam")

    if len(steam_documents):
        logging.info(f"The earliest is from {steam_documents[0].datetime}")
        logging.info(f"The latest is from {steam_documents[-1].datetime}")

    return steam_documents


def default_run():
    # TODO: replace with time of last source_df entry
    df = get_retrieved_dataframe()
    since_date_dict = last_df_date(df)

    logging.info(f"Last Steam comment in DB was at {since_date_dict['steam']}")

    # Steam
    documents_steam = get_steam_documents(
        SteamID.BORDERLANDS_3,
        since_date=since_date_dict["steam"],
    )

    logging.info(f"Last Reddit comment in DB was at {since_date_dict['reddit']}")
    # Reddit
    documents_reddit = get_reddit_documents(
        Subreddit.BORDERLANDS_3,
        since_date=since_date_dict["reddit"],
    )

    all_documents = documents_steam + documents_reddit

    add_documents_to_database(all_documents)


if __name__ == "__main__":
    default_run()
